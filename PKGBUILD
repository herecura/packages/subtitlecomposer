# Contributor: Mladen Milinkovic <maxrd2@smoothware.net>
# Maintainer: Mladen Milinkovic <maxrd2@smoothware.net>

pkgname=subtitlecomposer
pkgver=0.7.1
pkgrel=5
pkgdesc="A KDE subtitle editor"
arch=('x86_64')
url="https://invent.kde.org/multimedia/subtitlecomposer/"
license=('GPL')
depends=('kcoreaddons' 'ktextwidgets' 'kio' 'sonnet' 'kcodecs' 'kross' 'kxmlgui' 'ki18n' 'ffmpeg' 'openal')
makedepends=('extra-cmake-modules' 'jack' 'blas' 'xorg-server-xvfb')

optdepends=('kross-interpreters: Ruby and Python scripting support'
            'ruby: scripting'
            'python: scripting')

source=(
    "${pkgname}-${pkgver}.tar.gz::https://invent.kde.org/multimedia/${pkgname}/-/archive/v${pkgver}/${pkgname}-v${pkgver}.tar.gz"
    "https://invent.kde.org/multimedia/subtitlecomposer/-/commit/4f4f560e40ba0b760cf688eb024be3cc734ca347.patch"
    "https://invent.kde.org/multimedia/subtitlecomposer/-/commit/d8f9797d9c0d45fa9f4402f79c539544b74d2cc7.patch"
    "https://invent.kde.org/multimedia/subtitlecomposer/-/commit/12f4d7f49d0b1a7fc02b0836521a285e7b6bac9d.patch"
)
sha512sums=('3e78f4a255d260d13915c47d39a6485ba103743fdbd5623c3fd8d673ca4ed11f80335413202e8537c0bd602a3161442873a6ea74cc7be505af66236a4b12c067'
            '457dbeefeb3a9867836e980592c52b120aa4dd9205f317e13d35e8bcd9ca81ae843b77bb6fe097030ac88d3c4c61f38cac02533e5137e6c54a2339fdd60f0fc1'
            '35fe30d123aafef39f6684407bd9bbe86e85f9a9fcabf9cd91971dbd7b51acd9f48f1f2d2242f062cc02ba516f15aa8903beb210e74b7a465f45feaa5f0a4a6d'
            'e79cfd5aadd9117dc3d1c03ddd2f8ce2083cc279868abf63285b7248b0010eef1c7b8b717584750a148c8aee41378a7349ebfe29eaa93438b053fe40eb6e7a87')

prepare() {
    cd ${pkgname}-v${pkgver}
    patch -Np1 -i "$srcdir/4f4f560e40ba0b760cf688eb024be3cc734ca347.patch"
    patch -Np1 -i "$srcdir/d8f9797d9c0d45fa9f4402f79c539544b74d2cc7.patch"
    patch -Np1 -i "$srcdir/12f4d7f49d0b1a7fc02b0836521a285e7b6bac9d.patch"
}

build() {
    cd ${pkgname}-v${pkgver}
    cmake \
        -DCMAKE_BUILD_TYPE=Release \
        -DCMAKE_INSTALL_PREFIX=/usr \
        -DLIB_INSTALL_DIR=lib \
        -DKDE_INSTALL_USE_QT_SYS_PATHS=ON \
        -DBUILD_TESTING=OFF \
        -DAPP_VERSION="${pkgver}"
    make
}

package() {
    cd ${pkgname}-v${pkgver}
    make DESTDIR=${pkgdir} install
}
